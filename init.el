;; Performance tweaks
(setq gc-cons-threshold 100000000) ;; 100MB
(setq native-comp-always-compile t)
(setq native-comp-deferred-compilation t)

;; Repos setup
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)

;; straight
(setq straight-use-package-by-default t)
(setq straight-repository-branch "develop")
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;; Dispose customize output to dummy file
(setq custom-file (concat user-emacs-directory "/custom.el"))

;; obsolete fix for Emacs 28
(load-file (concat user-emacs-directory "obsolete-emacs28-fix.el"))


;;; Evil family
;; Needed for evil
;; TODO: Revisit this setting when Emacs 28 comes
(use-package goto-chg :ensure t)
(use-package undo-tree :ensure t :init (global-undo-tree-mode))

(use-package evil
  :ensure t
  :custom
  ;; For evil-collection
  (evil-want-keybinding nil)
  ;; For `cgn` command
  (evil-search-module 'evil-search)
  ;; For evil undo
  (evil-undo-system 'undo-tree)
  :config
  (evil-mode 1)
  (define-key evil-motion-state-map ";" 'evil-ex)
  (evil-select-search-module 'evil-search-module 'evil-search)
  (evil-define-key 'normal 'global
    "]c" 'diff-hl-next-hunk
    "[c" 'diff-hl-previous-hunk
    "\\hu" 'diff-hl-revert-hunk
    "\\;" "A;"
    "K" 'lsp-ui-doc-glance)
  (evil-define-key 'insert 'global
    (kbd "C-,") #'company-complete))

(use-package evil-collection
  :ensure t
  :config
  (evil-collection-init))
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))
(use-package evil-nerd-commenter
  :ensure t
  :config
  (evilnc-default-hotkeys))

;;; gui customizations
;; Map bold texts to semibold to avoid distraction
(defun replace-bold-with-semibold ()
  (mapc
   (lambda (face)
     (when (eq (face-attribute face :weight) 'bold)
       (set-face-attribute face nil :weight 'semibold)))
   (face-list)))

(use-package doom-themes
  :ensure t
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-treemacs-theme "doom-colors")
  (doom-one-brighter-comments t)
  :config
  (load-theme 'doom-one-light t)
  (doom-themes-visual-bell-config)
  (replace-bold-with-semibold)
  ;; Reset background color of comments
  ;; TODO use :custom-face to do this
  (custom-set-faces
   `(font-lock-comment-face ((t (:background ,(doom-color 'bg)))))))

(use-package doom-modeline
  :ensure t
  :hook (after-init . doom-modeline-mode))

(use-package centaur-tabs
  :ensure t
  :demand
  :init
  ;; Underline the selected tab
  (setq x-underline-at-descent-line t
        centaur-tabs-style "bar"
        centaur-tabs-set-bar 'under)

  :bind (("C-<prior>" . centaur-tabs-backward-tab)
         ("C-<next>" . centaur-tabs-forward-tab)
         ("C-<tab>" . centaur-tabs-forward-tab)
         ("C-<iso-lefttab>" . centaur-tabs-forward-group))
  :custom (centaur-tabs-height 36)
  (centaur-tabs-set-icons t)
  (centaur-tabs-set-modified-marker t)
  (centaur-tabs-show-navigation-buttons t)
  (centaur-tabs-cycle-scope 'tabs)
  :config
  (centaur-tabs-mode t))

;; Fuzzy jump to character
(use-package avy :ensure t
  :bind (("C-:" . avy-goto-char)))

;; Asynchronous relative line numbers
(use-package nlinum-relative
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'nlinum-relative-mode)
  :commands nlinum-relative-mode
  :config
  (nlinum-relative-setup-evil)
  :custom-face
  (linum ((t (:inherit default :foreground "dim gray" :strike-through nil :underline nil :slant normal :weight normal)))))

;; Ligatures and font
(cond ((string-match-p "x13" (system-name))
       (add-to-list 'default-frame-alist
                    '(font . "fira code-17")))
      ((string-match-p "rx570" (system-name))
       (add-to-list 'default-frame-alist
                    '(font . "fira code-13")))
      (t (add-to-list 'default-frame-alist
                      '(font . "fira code"))))
(use-package ligature
  :straight (ligature :type git :host github :repo "mickeynp/ligature.el")
  :defer 5
  :init
  (ligature-generate-ligatures)
  ;; Fira Code specific setup
  ;; Taken from <https://github.com/mickeynp/ligature.el/issues/8>
  (ligature-set-ligatures 'prog-mode '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
                                       ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
                                       "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
                                       "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
                                       "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
                                       "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
                                       "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
                                       "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
                                       "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
                                       "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%"))
  (global-ligature-mode t))

;; Sidebar
(use-package treemacs
  :ensure t
  :commands treemacs treemacs-select-window
  :bind (("M-0" . treemacs-select-window))
  :config
  (doom-themes-treemacs-config))
(use-package treemacs-evil
  :ensure t)
(use-package treemacs-projectile
  :ensure t)

;; Git hunks on fringe
(use-package diff-hl
  :ensure t
  :after evil
  ;; Defer loading after prog-mode hook
  :init
  (add-hook 'prog-mode-hook 'diff-hl--global-turn-on)
  :commands diff-hl--global-turn-on

  :config
  (setq diff-hl-loaded t)
  ;; Integrate with magit
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
  ;; Enable globally
  (global-diff-hl-mode))

;; Needed for centaur-tabs
(use-package all-the-icons :ensure t)

;; Welcome buffer
(use-package dashboard
  :ensure t
  :custom
  (dashboard-startup-banner 'logo)
  :config
  (dashboard-setup-startup-hook))


;;; lsp
(use-package lsp-mode
  :ensure t
  :commands lsp
  :custom
  ;; Was about to use "s-l", but it currently conflicts with i3wm
  (lsp-keymap-prefix "C-l")
  (lsp-clients-clangd-args '("--header-insertion=never"))
  (lsp-signature-doc-lines 2)
  (lsp-rust-analyzer-cargo-watch-args ["--target-dir=./rust-analyzer-target"])
  (lsp-rust-analyzer-cargo-load-out-dirs-from-check t)
  (lsp-rust-analyzer-proc-macro-enable t)
  :config
  (replace-bold-with-semibold)
  (add-to-list 'lsp-file-watch-ignored "[/\\\\]rust-analyzer-target\\'")
  ;; CMake LSP; To install server run `pip3 install cmake-language-server --user`
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection "cmake-language-server")
                    :activation-fn (lsp-activate-on "cmake")
                    :server-id 'cmake))
  ;; Custom lsp client for rust-analyzer with ./rust-analyzer-target as its target
  (lsp-register-client
   (make-lsp-client
    :new-connection (lsp-stdio-connection
                     (lambda ()
                       `(,(or (executable-find
                               (cl-first lsp-rust-analyzer-server-command))
                              (lsp-package-path 'rust-analyzer)
                              "rust-analyzer")
                         ,@(cl-rest lsp-rust-analyzer-server-command))))
    :major-modes '(rust-mode rustic-mode)
    :priority 10
    :initialization-options 'lsp-rust-analyzer--make-init-options
    :notification-handlers (ht<-alist '(("rust-analyzer/publishDecorations" . (lambda (_w _p)))))
    :action-handlers (ht ("rust-analyzer.runSingle" #'lsp-rust--analyzer-run-single)
                         ("rust-analyzer.debugSingle" #'lsp-rust--analyzer-debug-lens)
                         ("rust-analyzer.showReferences" #'lsp-rust--analyzer-show-references))
    :library-folders-fn (lambda (_workspace) lsp-rust-library-directories)
    ;; Here we set the environment variable
    :environment-fn (lambda () '(("CARGO_TARGET_DIR" . "./rust-analyzer-target")))
    :after-open-fn (lambda ()
                     (when lsp-rust-analyzer-server-display-inlay-hints
                       (lsp-rust-analyzer-inlay-hints-mode)))
    :ignore-messages nil
    :server-id 'rust-analyzer-mod
    :custom-capabilities `((experimental . ((snippetTextEdit . ,(and lsp-enable-snippet (featurep 'yasnippet))))))
    :download-server-fn (lambda (_client callback error-callback _update?)
                          (lsp-package-ensure 'rust-analyzer callback error-callback))))
  (add-hook 'js-mode-hook #'lsp)
  (add-hook 'c-mode-hook #'lsp)
  (add-hook 'c++-mode-hook #'lsp)
  (add-hook 'cmake-mode-hook #'lsp))
(use-package flycheck
  :ensure t)
(use-package flycheck-pos-tip
  :ensure t
  :config
  (flycheck-pos-tip-mode))
(use-package clang-format
  :ensure t)
(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))
(use-package lsp-ui
  :ensure t
  :after lsp-mode
  :custom
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-doc-enable nil)
  (lsp-ui-sideline-enable nil))
(use-package lsp-ivy
  :ensure t
  :after lsp-mode
  :config
  (evil-define-key 'normal 'global
    "\\s" 'lsp-ivy-workspace-symbol
    "\\f" 'lsp-ui-flycheck-list))
(use-package company
  :ensure t
  :config
  (setq lsp-enable-snippet t)
  ;; Use enter key to accept completion
  ;; From https://www.reddit.com/r/emacs/comments/3s3hdf/change_companymode_selectaccept_action_keybinding/cwtuncr?utm_source=share&utm_medium=web2x&context=3
  (let ((map company-active-map))
    (define-key map (kbd "RET") 'company-complete-selection)
    (define-key map (kbd "C-.") 'company-other-backend)))
(use-package company-tabnine
  :ensure t
  :config
  (defun add-company-tabnine () (interactive)
	 (add-to-list 'company-backends #'company-tabnine)))
(use-package lsp-treemacs
  :ensure t
  :custom
  ;; Put the lsp symbols window on right-hand side
  lsp-treemacs-symbols-position-params `((side . right) (slot . 1) (window-width . 35)))
(use-package lsp-dart
  :ensure t)
(use-package lsp-pyright
  :ensure t)


;;; NixOS integration
(if (string-match-p "nixos" (system-name))
    (use-package direnv
      :ensure t
      :config
      (direnv-mode)))


;;; misc
(use-package hl-todo
  :ensure t
  :init
  (add-hook 'prog-mode-hook (hl-todo-mode)))
(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))
;; For ivy history
(use-package ivy-prescient
  :ensure t
  :config
  (ivy-prescient-mode)
  :init
  ;; Fix error on lsp-ivy-workspace-symbol
  ;; As per https://github.com/emacs-lsp/lsp-ivy/issues/18
  (setq ivy-prescient-sort-commands '(:not swiper
                                           swiper-isearch
                                           counsel-imenu
                                           ivy-switch-buffer
                                           lsp-ivy-workspace-symbol
                                           ivy-resume
                                           ivy--restore-session
                                           counsel-switch-buffer)))
(use-package ivy
  :ensure t
  :config
  (ivy-mode))
(use-package counsel
  :ensure t)
(use-package which-key
  :ensure t
  :config
  (which-key-mode))
(use-package projectile
  :ensure t
  :config
  (projectile-mode)
  (define-key projectile-mode-map
    (kbd "C-c p") 'projectile-command-map))


;;; language modes
(use-package adoc-mode
  :ensure t)
(use-package cmake-mode
  :ensure t)
(use-package rust-mode
  :ensure t)
(use-package nix-mode
  :ensure t
  :mode "\\.nix\\'")
;; Dart lsp integration on NixOS
;; See https://github.com/emacs-lsp/lsp-dart/issues/90#issuecomment-759831455
(defun set-flutter-sdk-dir ()
  (direnv-update-environment)
  (setq lsp-dart-flutter-sdk-dir
        (string-trim-right (shell-command-to-string "echo $FLUTTER_SDK"))))
(use-package dart-mode
  :ensure t
  :custom
  (lsp-dart-dap-flutter-hot-reload-on-save t)
  :init
  (add-hook 'dart-mode-hook 'set-flutter-sdk-dir))
(use-package yaml-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))
(use-package rjsx-mode
  :ensure t)
(use-package js2-mode
  :ensure t)
(use-package dockerfile-mode
  :ensure t)


;;; heavy apps
(use-package magit
  :ensure t
  :commands magit magit-status
  :custom
  ;; Hide "^M" from the magit diff display
  (magit-diff-hide-trailing-cr-characters t))


;;; builtin tweaks
;; Declutter the UI
(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

;; enable vertical ruler
(add-hook 'prog-mode-hook (progn
			    (setq-default display-fill-column-indicator-column 100)
			    (global-display-fill-column-indicator-mode)))

;; Auto-close pair and highlight matching parenthesis
(electric-pair-mode 1)
(show-paren-mode 1)

;; For LSP to run smooth, as suggested by M-x lsp-doctor
(setq gc-cons-threshold 100000000) ;; 100MB
(setq read-process-output-max (* 1024 1024)) ;; 1MB

;; Don't use tabs
(setq indent-tabs-mode nil)

;; Always focus on the help window
(setq help-window-select t)

;; Suppress native-comp warnings
(setq native-comp-async-report-warnings-errors nil)

;; Underline and make URLs clickable
(global-goto-address-mode)

;; Disable scroll bars for new frames
;; As per https://emacs.stackexchange.com/a/23785
(defun disable-scroll-bars (frame)
  (modify-frame-parameters frame
                           '((vertical-scroll-bars . nil)
                             (horizontal-scroll-bars . nil))))
(add-hook 'after-make-frame-functions 'disable-scroll-bars)

;; Enable pixel-wise resize (for tiling script)
(setq frame-resize-pixelwise t)

;; Use ibuffer for buffer management
(global-set-key (kbd "C-x C-b") 'ibuffer)
;; Move backup files to somewhere else
(setq backup-directory-alist `(("." . "~/.cache/")))
;; Start server if not yet
(load "server")

;; Disable lockfiles
(unless (server-running-p) (server-start))
(setq create-lockfiles nil)
